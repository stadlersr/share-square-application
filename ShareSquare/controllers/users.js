var User = require('../models/user.js'),
    validationFuncs = require('../funclibs/validation.js');

var permissions = ['author','administrator'],
    requestCount = 0;
//Call Register Form
//Needs Page
exports.register = function(req, res){
    res.render("users/registration",{
        title:"Registration",
        permissions:permissions,
        flash:{},
        doc:{}
    });
};

//Send Register Form
exports.registerSave = function(req, res){
        let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    };
    
    let user = new User({
        username:req.body.username,
        password:req.body.password,
        email:req.body.email,
        name:req.body.name,
        created:req.body.created,
        updated:req.body.updated
    });
    if (user && user.permissions === "administrator"){
        user.permissions = req.body.permissions;
    }
    user.save(function(err){
        if(err){
            validationFuncs.errorHelper(err,function(errors){
                if(!errors){
                    errors = "Failed to save user";
                }
                res.render("users/registration",{
                   title:"Create an Account",
                   permissions:permissions,
                   flash:{error:errors},
                   doc:user
                });
            });
        }else{
            req.flash('notice','Page saved successfully');
            res.redirect('/users/login');
        }
    });
};

//Call Login Form
//Needs Page
//Step 3
exports.login = function(req,res){
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    };
    
        // req.flash('error','User not found');
        // res.redirect('users/login');
        res.render("users/login",{
            title:"Login",
            flash:flash
        });
};

//Password Recovery Form
//Needs Page
exports.recovery = function(req, res){
    res.render("users/recovery",{
        title:"Password Recovery",
            flash:{},
            docs:{}
    })   
};

exports.recoverySave = function(req, res){
    
};

//Display Profile
//Needs Page
exports.profile = function(req, res){
    res.render("users/profile",{
        title:"Profile",
            flash:{},
            docs:{}
    })   
};

//Profile Edit Form
//Needs Page
exports.editProfile = function(req, res){
    res.render("users/editProfile",{
        title:"Edit Profile",
            flash:{},
            docs:{}
    })   
};

//Send Profile Form
exports.editProfileSave = function(req, res){
    let id = req.params.id || 0;
    
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    User.findById(id, function(err,doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/users/editProfile');
        }else{
            doc.username = req.body.username;
            doc.password = req.body.password;
            doc.email = req.body.email;
            doc.name = req.body.name;
            doc.permissions = req.body.permissions;
            doc.created = req.body.created;
            doc.updated = req.body.updated;
            doc.save(function(err){
                if(err){
                    validationFuncs.errorHelper(err,function(errors){
                        if(!errors){
                            errors = "Failed to update profile";
                        }
                        res.render("users/editProfile",{
                           title:"Profile",
                           flash:{error:errors},
                           doc:doc
                        });
                    });
                }else{
                    req.flash('notice','Profile Update Successful');
                    res.redirect('/users/editProfile');
                }
            });
        }
    });
};

//???
exports.dashboard = function(req, res){
        res.render("users/dashboard",{
        title:"User Dashboard",
            flash:{},
            docs:{}
    });  
};

//Step 7
exports.logout = function(req,res){
    req.logout();
    res.redirect('/users/login');
};

//???
exports.allUsers = function(req, res){
    requestCount++;
    if(req.user.permissions === "administrator"){
            res.render("users/allusers",{
                title:"All Users",
                flash:{},
                docs:{}
            });
    }else if(requestCount === 3){
        res.redirect('/denied');
        requestCount === 0;
    }
};
exports.upload = function (req, res, next){
    console.log(req.file);
    next();
}
