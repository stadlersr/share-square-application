var Page = require('../models/page.js'),
    User = require('../models/user.js'),
    fs = require('fs'),
    validationFuncs = require('../funclibs/validation.js');

exports.index = function(req, res) {
    let flash = {
        notice: req.flash('notice')[0],
        error: req.flash('error')[0]
    }

    Page.find(function(err, docs) {
        if (err) {
            flash.error = "There was an error locating your pages";
            docs = [];
        }

        res.render("pages/index", {
            title: "ShareSquare",
            flash: flash,
            docs: {}
        });
    });
};

//Static Purpose of Site
//Needs Page
exports.about = function(req, res) {
    res.render("pages/about", {
        title: "About",
        flash: {},
        docs: {}
    })
};

//???
exports.content = function(req, res) {
    res.send("Content!");
};

//Static Denied Access Redirect
//Needs Page
exports.deny = function(req, res) {
    res.render("users/deny", {
        title: "Denied",
        flash: {},
        docs: {}
    })
};

//Content Create Form
//Needs Page
exports.createContent = function(req, res) {
    res.render("pages/createContent", {
        title: "Create Content",
        flash: {},
        docs: {}
    })
};

//Send Content Create Form
exports.createContentSave = function(req, res) {
    let flash = {
        notice: req.flash('notice')[0],
        error: req.flash('error')[0]
    }

    let page = new Page({
        title: req.body.contentTitle,
        path: req.file.path,
        fileType: req.file.mimetype,
        tag: req.file.filename
    });

    page.save(function(err) {
        if (err) {
            validationFuncs.errorHelper(err, function(errors) {
                if (!errors) {
                    errors = "Failed to save content";
                }
                res.render("pages/createContent", {
                    title: "Create Content",
                    flash: {
                        error: errors
                    },
                    doc: page
                });
            });
        }
        else {
            console.error('Image Uploaded');
            res.redirect('/users/view');
        }
    });
};

//Edit Content Form
//Needs Page
exports.editContent = function(req, res) {
    res.render("pages/editContent", {
        title: "Edit Content",
        flash: {},
        docs: {}
    })
};

//Send Edit Content Form
exports.editContentSave = function(req, res) {
    let tag = req.params.tag || 0;

    let flash = {
        notice: req.flash('notice')[0],
        error: req.flash('error')[0]
    }

    Page.findOne({
        tag: tag
    }, function(err, doc) {
        if (err) {
            console.log(err);
            req.flash('error', 'Content not found');
            res.redirect('/users/view');
        }
        else {
            doc.title = req.body.title;
            doc.tag = tag;
            doc.save(function(err) {
                if (err) {
                    validationFuncs.errorHelper(err, function(errors) {
                        if (!errors) {
                            console.log("failed to update");
                            errors = "Failed to update content";
                        }
                        res.redirect("users/view");
                    });
                }
                else {
                    console.log("Update Successful");
                    req.flash('notice', 'Content Update Successful');
                    res.redirect('/users/view');
                }
            });
        }
    });
};

//???
exports.view = function(req, res) {
    let tag = req.params.tag || 0;

    let flash = {
        notice: req.flash('notice')[0],
        error: req.flash('error')[0]
    }

    Page.findById(tag, function(err, doc) {
        if (err) {
            req.flash('error', 'Page not found');
            res.redirect('/');
        }
        else {
            console.log(doc);
            res.render("pages/view", {
                title: "View Page",
                flash: flash,
                doc: doc
            });
        }
    });
};

//Display 
exports.viewAll = function(req, res) {
    let flash = {
        notice: req.flash('notice')[0],
        error: req.flash('error')[0]
    }

    let conditions = {};

    Page.find(conditions, function(err, docs) {
        if (err) {
            flash.error = "There was an error locating your pages";
            docs = [];
        }

        res.render("pages/viewAll", {
            title: "List Content",
            flash: flash,
            docs: docs
        })
    });
};

//Send Delete Content
exports.deleteContent = function(req, res) {
    let tag = req.params.tag || 0;

    Page.findOne({
        tag: tag
    }, function(err, doc) {
        if (err) {
            req.flash('error', 'Title not found');
            res.redirect('/users/view');
        }
        else {
            doc.remove(function(err) {
                if (err) {
                    req.flash('error', 'Error deleting page');
                    res.redirect('/users/view');
                }
                else {
                    req.flash('notice', doc.title + " has been deleted");
                    res.redirect("/users/view");
                }
                fs.unlink(doc.path, function(err) {
                    if (err) {
                        return console.error(err);
                    }
                });
            });
        }
    });

};

//???
exports.allUsers = function(req, res) {
    res.send("allUsers!");
};
