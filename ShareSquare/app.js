var express = require('express'),
  session = require('express-session'),
  bodyParser = require("body-parser"),
  logger = require("express-logger"),
  cookieParser = require("cookie-parser"),
  flash = require("express-flash"),
  mongoose = require("mongoose"),
  fs = require('fs'),
  multer = require('multer'),
  upload = multer({
    dest: './uploads/'
  }),
  pages = require("./controllers/pages.js"),
  users = require("./controllers/users.js"),
  app = express(),
  sessionStore = new session.MemoryStore,
  uploads = require("./controllers/uploads.js"),
  //passport for user login
  passport = require("passport"),
  LocalStrategy = require("passport-local"),
  User = require("./models/user");

//configuratin goes here
app.set('port', process.env.PORT || 3000);
app.set('views', './views');
app.set('view engine', 'pug');

//Middleware
app.use(express.static('public')); //set a static route
app.use(logger({
  path: "logfile.txt"
}));
app.use(bodyParser.urlencoded({
  extended: false
})); // parse post boddy (.json()) is also available
app.use(cookieParser('secret'));
app.use(session({
  cookie: {
    maxAge: 60000
  },
  store: sessionStore,
  saveUninitialized: true,
  resave: 'true',
  secret: 'secret'
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(function(username, password, done) {
  User.findOne({
    username: username
  }, function(err, doc) {
    if (err) { //database error
      console.log("Login: database error");
      return done(err, false);
    }
    if (!doc) { //user not found
      console.log("Login: user not found");
      return done(null, false, {
        error: 'User or password not found.'
      });
    }
    else {
      if (doc.password === doc.hashPassword(password)) { //success
        console.log("Login: success");
        return done(null, doc);
      }
      else { //bad password
        console.log("Login: bad password");
        return done(null, false, {
          error: 'User or password not found.'
        });
      }
    }
  });
}));
//Step 8 -with commented out stuff
app.use(function(req, res, next) {
  //Step 9
  res.locals.user = req.user;

  //delete the line below and uncomment out the if/else statement for login security
  // next();
  if (req.user === undefined && req.path !== "/users/login" && req.path !== "/registration" && req.path !== "/users/recovery" && req.path !== "/" && req.path !== "/about") {
    res.redirect("/users/login");
  }
  else {
    next();
  }
});
app.use(flash());

//Step 6
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});
//mongoose connectivity
mongoose.connect(process.env.IP + "/local");
mongoose.connection.on('connected', function() {
  console.log("Connected to MongoDB @" + process.env.IP + "/local");
});
mongoose.connection.on("SIGINT", function() {
  console.log('MongoDB disconnected due to Application Exit');
  process.exit(0);
});

//public level
app.get('/', pages.index);

app.get('/about', pages.about);
app.get('/content', pages.content);
app.get('/denied', pages.deny);
app.get('/registration', users.register);
app.post('/registration', users.registerSave);

//Adding login/logout routes
app.get('/users/login', users.login);
app.get('/logout', users.logout);
app.get('/users/recovery', users.recovery);
app.post('/users/recovery', users.recovery);

//user routes
//entry level

//private level
app.get('/users/dashboard', users.dashboard);
app.get('/users/profile', users.profile);
app.get('/users/profile', users.editProfile);
app.post('/users/profile', users.editProfile);
app.get('/uploads/:id', uploads.getImage);

app.post('/users/create', upload.single('content'), function(req, res, next) {
    console.log(req.file);
    next()
  }) //multer stuff
app.post('/users/create', pages.createContentSave);

app.get('/users/create', pages.createContent);
//app.get('/users/edit', pages.editContent);
app.post('/users/edit/:tag', pages.editContentSave);

app.get('/pages/view/:tag', pages.view);
app.get('/pages/delete/:tag', pages.deleteContent);
app.get('/users/view', pages.viewAll);

//admin routes
app.get('/users/all', users.allUsers);

//Step 4 for passport
app.post('/users/login', passport.authenticate('local', {
  successRedirect: '/users/dashboard', //where we go on successful login
  failureRedirect: '/users/login', //where to go when we fail to login
  failureFlash: true //use flash messages
}));

//start the application
app.listen(process.env.PORT, function() {
  console.log('ShareSquare app listening on port ' + process.env.PORT + '!');
});