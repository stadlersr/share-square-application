var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
    
var PageSchema = new Schema({
    title:{
        type:String
    },    
    path:{
        type:String
    },
    fileType:{
        type:String
    },
    tag:{
        type:String
    }
});

var Page = mongoose.model('Page',PageSchema);
module.exports = Page;