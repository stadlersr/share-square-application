Task Listing:

Here's a breakdown of what I think each controller should handle and how we should organize the and name the routes/pages.
Items with a * should have their own PUG file
Pages
	*index
	*about
	*createContent
	createContentSave
	deleteContent
	*viewContent
	*editContent
	editContentSave
	*contentView


Users
	*register
	registerSave
	*login
	loginSave
	*recover
	recoverSave
	*deny
	*profile
	*profileEdit
	profileEditSave
	logout